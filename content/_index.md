## About

Energy Models is a [Julia](https://julialang.org/) package for planning large-scale energy systems with high shares of renewable energy by co-optimizing capacities and dispatch of wind turbines, solar panels, storage, transmission and thermal generation units.

Development takes place in a public Gitlab repository: [coroa/energymodels](https://gitlab.com/coroa/energymodels/).

It is closely tied to [PyPSA](https://pypsa.org/) and its models, especially [PyPSA-Eur](https://github.com/PyPSA/pypsa-eur/).

It additionally draws inspiration from:

* [PowerModels.jl](https://github.com/lanl-ansi/PowerModels.jl)
* [PowerSystems.jl](https://github.com/NREL/PowerSystems.jl)/[PowerSimulations.jl](https://github.com/NREL/PowerSimulations.jl)
* [PandaPower](https://pandapower.org/)

The project is developed under the umbrella of the [Prototypefund](https://prototypefund.de/project/energymodels/) funded by the [Federal Ministry of Education and Research (Germany)](https://bmbf.de/). The project was initiated at the [Energy System Modelling group](https://www.iai.kit.edu/english/2338.php) at the Institute for Automation and Applied Informatics at the [Karlsruhe Institute of Technology](http://www.kit.edu/english/index.php).
