---
title: "energymodels.de on gitlab pages"
subtitle: "Hugo via Gitlab CI on custom domain"
date: 2019-04-09
tags: [tech,work]
---
Setting up the blog for the Energy Models project in the Prototypefund was a _bliss_ with Gitlab pages. Their project templates (`New project`->`Create from template`->`Pages/Hugo`) come with the CI configuration to use their shared runners for generating a static webpage with posts, pages and tags from simple markdown files managed in the [energymodels-pages](https://gitlab.com/coroa/energymodels-pages/) git repository.

Running `hugo server` locally gives a helpful live preview of the blog.

The `your-server.de` domain administration was powerful and simple enough to finish the `custom domain` dance described in [gitlab's docs](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html) without any serious blockers.
